function DataStorage() {

    this.save = function save(expanses) {

        // On converti la liste des dépenses au format JSON
        const json = JSON.stringify(expanses)


        localStorage.setItem("expanses", json);
    };

    this.read = function read() {
        // extraire les données
        const jsonData = localStorage.getItem("expanses");

        // convertir json en javascript
        const data = JSON.parse(jsonData)

        return {
            expanseList: data,
        };
    };


}