function Application(data) {
    /**
     * Declaration des propriétés
     * 
     * @var {Person[]}
     * @var {Expanse[]}
     * 
     */

    //liste des personnes
    const personList = [];

    //liste recapitulative des dépenses
    const expanseList = [];

    /**
       * @returns {boolean} true if the name is successfully registered,
       *                    false is the name is aldreay taken
       */
    this.registerPerson = function registerPerson(pseudo) {

        if (pseudo === "") {
            console.warn("Remplissez le champs de pseudo pour enregistrer");
            return;
        };

        for (const person of personList) {
            if (person.getName() === pseudo) {
                console.warn(`Le pseudo ${pseudo} est déja utilisé.`);
                return false;
            }

        }
        personList.push(new Person(pseudo));
        // console.log("Nombre de personnes enregitrées: " + this.getPersonsCount());

        return true;
    };


    /**
       * @returns {boolean} true if the name is successfully registered,
       *                    false is the name is aldreay taken
       */
    this.registerExpanse = function registerExpanse(pseudo, mount, label, date) {




        if (typeof mount !== "number" || mount <= 0 || mount >= 1000 || Number.isNaN(mount)) {
            console.warn("Le montant doit être un nombre positif, non nul, inférieur à 1000");
            return;
        }
        if (label === "") {
            console.warn("Definissez un label");
            return;
        }
        if (date === "") {
            console.warn("Definissez une date");
            return;
        }
        //search if the personn is in the list
        var userExist = false;
        for (const person of personList) {
            if (person.getName() === pseudo) {
                userExist = true;
            }
        }

        // if the person is in the list
        if (userExist === true) {
            expanseList.push(new Expanse(pseudo, mount, label, date));
            return true;
        }

        // if it's not in the list
        else {
            console.warn("Le payeur doit etre dans la liste");
            return false;
        }
    };

    this.setExpansesTotalAmount = function setExpansesTotalAmount() {
        //Montant total des dépenses ( au début à zéro;)
        var totalCount = 0

        for (const expanse of expanseList) {
            totalCount += expanse.getMount();
        }
        totalCount = Math.round(totalCount * 10) / 10;
        return totalCount;
    };

    this.getPersonsCount = function getPersonsCount() {
        return personList.length;
    };

    this.getExpensesCount = function getExpensesCount() {
        return expanseList.length;

    };

    this.getAllExpansesList = function getAllExpansesList() {
        return expanseList.slice();
    };
    this.getAllPersonsList = function getAllPersonsList() {
        return personList.slice();
    };
    this.setPriceForOne = function setPriceForOne() {
        var priceForOne = 0;
        priceForOne = this.setExpansesTotalAmount() / this.getPersonsCount();
        priceForOne = Math.round(priceForOne * 10) / 10;
        return priceForOne
    };

    this.setPersonExpansesList = function setPersonExpansesList(person) {

    };

    this.setPersonTotalCount = function setPersonTotalCount(personExpList) {

    };

    this.setPersonSold = function setPersonSold(persExpCount) {

    };





    if (!data || !data.expanseList || !Array.isArray(data.expanseList)) {
        console.warn(
            "Impossible de charger les données. (inexistantes ou non valides"
        );
        return;
    }

    for (const expanseShape of data.expanseList) {
        this.registerPerson(expanseShape.person);

        this.registerExpanse(
            expanseShape.amount,
            expanseShape.person,
            expanseShape.label,
            expanseShape.date
        );
    }

}