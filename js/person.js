function Person(newName) {

    // Declaration des propriétés
    /**
     * @private
     * @var {string}
     */
    var name;


    // Declaration des Méthodes

    this.getName = function getName() {
        return name;
    }

    this.setName = function setName(newName) {

        if (typeof newName !== "string") {
            console.warn("Le name doit être une chaine de caractères");
            return;
        }
        newName = newName.trim();

        if (newName === "") {
            console.warn("newName ne doit pas être vide");
            return;
        }

        name = newName;

    }

    // Instantiation
    if (newName) {
        this.setName(newName);

    }


}
