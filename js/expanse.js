function Expanse(newPerson, newMount, newLabel, newDate) {

    // Declaration des propriétés
    /**
     * @private
     * @var {string} pseudo of the person who paid the expanse
     */
    var person;
    /**
     * @private
     * @var {number} 
     */
    var mount;
    /**
     * @private
     * @var {string} 
     */
    var label;
    /**
     * @private
     * @var {string} 
     */
    var date;


    // Déclaration des méthodes

    this.getMount = function getMount() {
        return mount;
    };

    this.setMount = function setMount(newMount) {

        if (typeof newMount !== "number" || newMount < 0 || mount >= 1000 || Number.isNaN(newMount)) {
            console.warn("Le montant doit être un nombre, positif, inférieur à 1000, non NaN");
            return;
        }
        else {

            mount = newMount;
        }

    };

    this.getPersonMount = function getPersonMount() {
        return person;
    };

    this.setPersonMount = function setPersonMount(newPerson) {
        if (typeof newPerson !== "string") {
            console.warn("newPerson doit être une chaine de caractères");
            return;
        }
        if (newPerson === "") {
            console.warn("newPerson ne doit pas être vide");
            return;
        }
        // code de Pierre, personn = objet
        //     if (!(person instanceof Person)) {
        //         console.warn(`setPerson(): erreur, paramètre person invalide. 
        //   Une instance de Person est attendue, vous avez donné : "${person}"`);
        //       }

        person = newPerson;
    };

    this.getLabelMount = function getLabelMount() {
        return label;
    };

    this.setLabelMount = function setLabelMount(newLabel) {
        if (typeof newLabel !== "string") {
            console.warn("newLabel doit être une chaine de caractères");
            return;
        }
        if (newLabel === "") {
            console.warn("newLabel ne doit pas être vide");
            return;
        }
        label = newLabel
    };


    this.getDateMount = function getDateMount() {
        return date;
    };

    this.setDateMount = function setDateMount(newDate) {
        if (typeof newDate !== "string") {
            console.warn("person doit être une chaine de caractères");
            return;
        }
        if (newDate === "") {
            console.warn("newDate ne doit pas être vide");
            return;
        }
        date = newDate
    };

    this.toJSON = function () {
        return {
            mount: mount,
            label: label,
            date: date,
            person: person,

        };
    }
    // Instantiation
    if (newPerson, newMount, newLabel, newDate) {
        this.setPersonMount(newPerson);
        this.setMount(newMount);
        this.setLabelMount(newLabel);
        this.setDateMount(newDate);

    }




}