
const storage = new DataStorage();
const initialData = storage.read();
const myApp = new Application(initialData); // "hydratation" avec les données initiales


document.addEventListener("DOMContentLoaded", function () {
    const $pseudoForm = document.querySelector("#pseudo-form");
    const $expanseForm = document.querySelector("#expanse-form");
    const $totalCount = document.querySelector(".total-count");
    const $totalPerson = document.querySelector(".total-person");
    const $totalExpanse = document.querySelector(".total-expanse");
    const $pricePerPerson = document.querySelector(".price-person");
    const $btnCalcRefund = document.querySelector(".calc-refund");

    // Rendu initial
    updateExpansesList();
    updatePersonsList();


    $pseudoForm.addEventListener("submit", function (event) {
        event.preventDefault(); //empeche le rechargement de la page

        // On stocke les données du formulaire HTML (this), dans un objet
        // @see https://developer.mozilla.org/en-US/docs/Web/API/FormData
        const data = new FormData(this);

        // On extrait la valeur qui nous intéresse (ici le champ "person-name")
        const pseudo = data.get("pseudo-form");


        // On essaye d'enregistrer ce pseudo dans l'app (couche modele)
        if (myApp.registerPerson(pseudo) === true) {

            $totalPerson.textContent = myApp.getPersonsCount();
            $pricePerPerson.textContent = myApp.setPriceForOne() + " €";
            updatePersonsList();


            alert(`${pseudo} à bien été enregistré`);
            this.reset();
            jQuery("#person-modal").modal("hide");
        } else {
            alert(`${pseudo} non enregistré, veuillez recommencer`);
        }
    });

    $expanseForm.addEventListener("submit", function (event) {
        event.preventDefault(); //empeche le rechargement de la page

        // On stocke les données du formulaire HTML (this), dans un objet
        // @see https://developer.mozilla.org/en-US/docs/Web/API/FormData
        const data = new FormData(this);

        // On extrait la valeur qui nous intéresse 
        const pseudoExpanse = data.get("pseudoExpanseInput");
        const mountExpanse = Number(data.get("mountExpanseInput"));
        const labelExpanse = data.get("labelExpanseInput");
        const dateExpanse = data.get("dateExpanseInput");


        // On essaye d'enregistrer cette dépense dans l'app (couche modele)
        if (myApp.registerExpanse(pseudoExpanse, mountExpanse, labelExpanse, dateExpanse) === true && (!(Number.isNaN(mountExpanse)))) {

            $totalExpanse.textContent = myApp.getExpensesCount();
            $totalCount.textContent = myApp.setExpansesTotalAmount() + " €";
            $pricePerPerson.textContent = myApp.setPriceForOne() + " €";
            updateExpansesList();

            alert(`La dépense à bien été enregistrée`);
            // formulaires remis a état initiaux
            this.reset();
            // fermeture de la modal
            jQuery("#expense-modal").modal("hide");


            // On persiste (stock) les données (la liste des dépenses)
            // const expanses = myApp.getAllExpansesList();
            // storage.save(expanses);

        } else {
            alert('La dépense n\'a pas été ajoutée');
        }
    });


    $btnCalcRefund.addEventListener("click", function (event) {
        event.preventDefault(); //empeche le rechargement de la page



    });
})


function updateExpansesList() {

    // on selectionne le ul correspondant
    const $ul = document.querySelector("#expanses-list ul")

    // je vide le html de la liste actuelle 
    // (astuce de bourrin, conssomme de la mémoire)
    $ul.innerHTML = "";

    //  on sort lal iste des dépenses de la couche modèle
    const list = myApp.getAllExpansesList();

    // pour chaque dépense ...
    for (const expanse of list) {
        // ... on crée un li et ce qu'il contient
        const html = createHTML(expanse);
        const $li = document.createElement("li");
        $li.innerHTML = html;
        //  ... on l'ajoute à la suite au HTML dans la liste ul
        $ul.append($li);

    }
};
function createHTML(expanse) {
    return `
            <ul class="list-group border-0 ">
                <li
                    class="d-flex list-unstyled justify-content-center align-items-center list-group-item  row  border-bottom border-top-0 border-left-0 border-right-0 pl-2 pr-2 pt-0 pb-0 ml-0 mr-0 mb-1 mt-1">
                    <div class="d-flex flex-column align-items-center p-0 ml-0 mr-3 border-bottom-0 col col-2">
                        <div class="d-flex ">
                            <div class="pb-2  col-5  ">
                                <strong>
                                ${expanse.getPersonMount()}
                                </strong> </div>
                        </div>
                        <small class="pt-1 border-top border-info">
                        ${expanse.getDateMount()}
                        </small>
                    </div>
                    <small class="col m-0 d-flex justify-content-center align-items-center ">
                    ${expanse.getLabelMount()}
                    </small>
                    <div class="d-flex flex-column align-items-end col m-0 p-0">
                        <div class="  d-flex m-0 p-0" aria-label="Basic example">
                            <button type="button" class="ml-1 btn btn-success btn-sm ">E</button>
                            <button type="button" class="ml-1 btn btn-danger btn-sm ">S</button>

                        </div>
                        <h4 class="mt-2  "><strong>
                        ${expanse.getMount()}€
                        </strong></h4>
                    </div>
                </li>
            </ul>
            
        
    `

};

function updatePersonsList() {

    const $select = document.querySelector("#exampleFormControlSelect1")


    $select.innerHTML = "";


    const personsList = myApp.getAllPersonsList();


    for (const person of personsList) {

        const html2 = createHTMLOption(person);
        const $option = document.createElement("option");
        $option.innerHTML = html2;

        $select.append($option);

    }
};
function createHTMLOption(person) {
    return `
    ${person.getName()}
`

}





